import React from 'react';
import axios from 'axios';

const redirect_uri = document.querySelector('script[name="REDIRECT_URI"]').innerText
const client_id = 'Iv1.8a2c4d4d8956f0ca'
const gatekeeper_url = document.querySelector('script[name="GATEKEEPER_URL"]').innerText

const oauth_url = `https://github.com/login/oauth/authorize?client_id=${client_id}&redirect_uri=${redirect_uri}&state=foo`


function getUser() {
  return sessionStorage.getItem('user');
}


function BadassButton(props) {
  return <button onClick={(e) => {props.onClick(e)}}>{props.text}</button>
}


function RepoList(props) {
  const repos = props.repos;
  const listItems = repos.map((repo) =>
    <li key={repo.name}>
      {repo.name} - <small>{repo.description}</small>
    </li>
  );
  return (
    repos ? (
      <div>
        <h2>Repositories</h2>
        <ul>{listItems}</ul>
      </div>
    ) : null
  );
}


class App extends React.Component {
  constructor(props) {
    super(props);
    let url = new URL(window.location.href)
    let code = url.searchParams.get('code')
    let state = url.searchParams.get('state')
    this.state = {
      user: JSON.parse(getUser()),
      oauthRedirect: {
        code: code,
        state: state
      },
      loading: !!code,
      repos: [],
    }
  }

  handleLoginButtonClick(e) {
    window.location = oauth_url
  }

  handleLogoutButtonClick(e) {
    sessionStorage.removeItem('user')
    sessionStorage.removeItem('access_token')
    this.setState({
      user: null,
      oauthRedirect: {
        code: null,
        state: null
      },
      repos: [],
    });
  }

  getAccessToken(code, state) {
    axios.get(`${gatekeeper_url}/${code}`)
    .then((res) => {
      sessionStorage.setItem('access_token', res.data.token);

      window.history.pushState({}, '', '/');

      this.setState({loading: true})

      let getUser = axios({
        method: 'get',
        url: 'https://api.github.com/user',
        headers: {
          'Authorization': `token ${res.data.token}`
        }
      })
      // .then((res) => {
      // })

      let getRepos = axios({
        method: 'get',
        url: 'https://api.github.com/user/repos',
        headers: {
          'Authorization': `token ${res.data.token}`
        },
        params: {
          visibility: 'public',
          affiliation: 'owner',
        }
      })
      // .then((res) => {
      //   this.setState({
      //     repos: res.data,
      //   });
      // })

      axios.all([getUser, getRepos]).then(axios.spread((userRes, reposRes) => {
        sessionStorage.setItem('user', JSON.stringify({username: userRes.data.login}))
        this.setState({
          user: JSON.parse(sessionStorage.getItem('user')),
          repos: reposRes.data,
          loading: false,
        });
      }))
    })
  }

  componentDidMount() {
    const user = this.state.user;
    if (!user && this.state.oauthRedirect.code) {
      this.getAccessToken(this.state.oauthRedirect.code, this.state.oauthRedirect.state)
    }
  }

  render() {
    let user = this.state.user;
    let isLoading = this.state.loading;
    return (
      <div>
        {isLoading ? (<p>Loading...</p>) : null}
        {user ? (
            <div>
              <h1>Hello {user.username}!</h1>
              <BadassButton onClick={(e) => this.handleLogoutButtonClick(e)} text="Log out" />
              <RepoList repos={this.state.repos} />
            </div>
        ) : (
          isLoading ? null : (<BadassButton onClick={(e) => this.handleLoginButtonClick(e)} text="Login with Github" />)
        )}
      </div>
    );
  }
}

export default App;
